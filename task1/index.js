const getInvertedNumber = (number) => {};

console.log("123 -> 321", getInvertedNumber(123) === 321);
console.log("-25 -> -25", getInvertedNumber(-25) === -25);
console.log("3 -> 3", getInvertedNumber(3) === 3);
console.log("100 -> 1", getInvertedNumber(100) === 1);
